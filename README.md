# Starter projects

This repository contains all starter projects that are used in our documentation and are offered to the third-party developers to jumpstart their projects.

To use these, simply follow the instructions in each README file of the main folders.

The starter projects are organized in the following way:

* [Simple Component](Simple%20Component/) —
This project contains a simple Hello World component and a gentle introduction to component development and is integrated in the form of a `HelloWorld` addin.
* [Advanced Component](Advanced%20Component/) —
This project contains a Hello Universe component with some component controls pre-configured with it and is integrated in the form of a `HelloUniverse` addin. 
* [Custom Data Type](Custom%20Data%20Type/) —
This project contains a custom data type with some components to create and inspect it.
* [Unit Tests](Tests/UnitTests/) —
This project contains sample unit tests for data types, components and test framework files.