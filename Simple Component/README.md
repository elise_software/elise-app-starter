# Welcome to ELISE component development

## Developer portal

Full guide explaining how to create your first ELISE component is available
[on the developer portal](https://portal.elise.de/#/dev-docs/latest/990806034).

## Quick start

This guide will allow you to add a new `Hello World` component to ELISE.

It shows you how to

* setup an [add-in project](https://portal.elise.de/#/dev-docs/latest/1621884929)
* create a [component](https://portal.elise.de/#/dev-docs/latest/990642184)
* add [icons](https://portal.elise.de/#/dev-docs/latest/1614905369) for the add-in and components

1. Make sure that ELISE is installed to `C:\Program Files\ELISE`
    * If it's installed to a different location, you need to update `LocalEliseLocation` variable the `.csproj` file and `executablePath` in `Properties/launchSettings.json`
2. Open `EliseSamples.sln`
3. Set `SimpleComponent` as the startup project
4. Click on Run button to build the project and start debug session
    * ELISE starts automatically whenever you start debug session
    * In DEBUG mode the project is built to `C:\ProgramData\ELISE\Addins` - this is the default folder for ELISE addins
5. In ELISE you should see a new component in Utilities tab  
![alt text](Images/Image1.png "Title")
6. Add this component on canvas, attach panels and a toggle
![alt text](Images/Image2.png "Title")
7. Put a breakpoint to `SolveInstance` method of the `Hello World` component  
![alt text](Images/Image3.png "Title")
8. Adjust the inputs on canvas to debug the component  
![alt text](Images/Image4.png "Title")  
![alt text](Images/Image5.png "Title")

## Contents of the project

- `HelloWorld.cs` - `Hello World` component
- `HelloWorld.addin` - metadata of the add-in
- `Properties\AssembyInfo.cs` - metadata of the project
- `Icons\` - icons for the add-in and component