﻿using Elise.Core.Graph.Data;
using Elise.Core.Implementation.Graph;
using Elise.Core.Implementation.UI;
using Elise.DataTypes;
using System;
using System.Runtime.InteropServices;

namespace SimpleComponent
{
    [Guid("FD1CC0CF-BC34-480A-B4B1-543D6A1891EA")]
    public sealed class HelloWorld : Component
    {
        public HelloWorld()
            : base("Hello World")
        {
            Category = Categories.Utilities;
            Subcategory = Subcategories.Utilities.Text;
            Description = "A component that outputs a custom Hello World! message.";
            GuiPriority = 1;

            InputParameterManager.AddParameter<EliseString>("Message", "The message to be added after the Hello, World! message.");
            InputParameterManager.AddParameter<EliseBool>("New line", "If true, a newline will be added between the message and Hello, World!.");

            OutputParameterManager.AddParameter<EliseString>("Text", "The custom Hello, World! message.");
        }

        protected override void SolveInstance(IDataAccess dataAccess)
        {
            bool isDataSuccess = dataAccess.GetData(0, out EliseString message);
            isDataSuccess &= dataAccess.GetData(1, out EliseBool newline);

            if (!isDataSuccess)
                return;

            var separator = newline ? Environment.NewLine : " ";
            var newString = $"Hello, World!{separator}{message}";
            dataAccess.SetData(0, newString);
        }
    }
}