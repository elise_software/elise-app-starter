﻿using Elise.Core.Implementation.ApplicationService;
using System.Reflection;

[assembly: AssemblyEliseAddin("0cc8c365-dca4-43cf-acc5-f3db891a3e57")] // make sure that this GUID matches the one in HelloWorld.addin file

[assembly: AssemblyTitle("Simple Component")]
[assembly: AssemblyVersion("1.0.0.0")]

[assembly: System.Runtime.Versioning.SupportedOSPlatform("windows10.0.15063")] 