﻿using AdvancedComponent.Components.Utilities.Text;
using Elise.Localization;
using Elise.Wpf.DirectX.Attributes;
using Elise.Wpf.DirectX.Canvas.Controls.Component;
using Elise.Wpf.DirectX.Canvas.Controls.ComponentControls.Builders;
using Elise.Wpf.DirectX.Controls;
using Elise.Wpf.DirectX.Extensions;

namespace AdvancedComponent.Controls.Utilities.Text
{
    [ViewFor(typeof(HelloUniverse))]
    public sealed class HelloUniverseControl : ComponentWithComponentControls
    {
        public HelloUniverseControl(HelloUniverse component)
            : base(component)
        {
        }

        protected override void ConfigureControls(StackPanel container, Builder builder)
        {
            var dropdownComponentControl = builder
                .CreateDropDown()
                .AddItem(new LocalizableString(nameof(AdvancedComponent.Resources.EnglishLanguageTypeName), typeof(AdvancedComponent.Resources)))
                .AddItem(new LocalizableString(nameof(AdvancedComponent.Resources.GermanLanguageTypeName), typeof(AdvancedComponent.Resources)))
                .AddItem(new LocalizableString(nameof(AdvancedComponent.Resources.FrenchLanguageTypeName), typeof(AdvancedComponent.Resources)))
                .AddItem(new LocalizableString(nameof(AdvancedComponent.Resources.DutchLanguageTypeName), typeof(AdvancedComponent.Resources)))
                .AddItem(new LocalizableString(nameof(AdvancedComponent.Resources.UkrainianLanguageTypeName), typeof(AdvancedComponent.Resources)))
                .AddItem(new LocalizableString(nameof(AdvancedComponent.Resources.AlienLanguageTypeName), typeof(AdvancedComponent.Resources)))
                .BuildForEliseIntParameter(0);

            var toggleComponentControl = builder
                .CreateBooleanToggle()
                .BuildForEliseBoolParameter(1);

            var sliderComponentControl = builder
                .CreateNumberSlider()
                .SetMinimum(0)
                .SetMaximum(5)
                .SetDecimalPlaces(0)
                .BuildForEliseIntParameter(2);

            container.AddChild(dropdownComponentControl);
            container.AddChild(toggleComponentControl);
            container.AddChild(sliderComponentControl);
        }
    }
}
