﻿using Elise.Core.Implementation.ApplicationService;
using System.Reflection;

[assembly: AssemblyEliseAddin("caf796d5-3132-4919-ae60-c9862a4cd3d3")] // make sure that this GUID matches the one in AdvancedComponent.addin file

[assembly: AssemblyTitle("Advanced Component")]
[assembly: AssemblyVersion("1.0.0.0")]

[assembly: System.Runtime.Versioning.SupportedOSPlatform("windows10.0.15063")] 