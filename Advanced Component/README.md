# Welcome to ELISE component development

## Developer portal

Full guide explaining advanced component development is available
[on the developer portal](https://portal.elise.de/#/dev-docs/latest/990707758).

## Quick start

This guide will allow you to add a new `Hello Universe` component to ELISE.

It shows you how to

* add [component controls](https://portal.elise.de/#/dev-docs/latest/1634959389) (sliders, toggles, dropdowns) to components
* make components [localizable](https://portal.elise.de/#/dev-docs/latest/1621884960) (using `LocalizableString`)
* create [help files](https://portal.elise.de/#/dev-docs/latest/1607401502) for components

1. Make sure that ELISE is installed to `C:\Program Files\ELISE`
    * If it's installed to a different location, you need to update `LocalEliseLocation` variable the `.csproj` file and `executablePath` in `Properties/launchSettings.json`
2. Open `EliseSamples.sln`
3. Set `AdvancedComponent` as the startup project
4. Click on Run button to build the project and start debug session
    * ELISE starts automatically whenever you start debug session
    * In DEBUG mode the project is built to `C:\ProgramData\ELISE\Addins` - this is the default folder for ELISE addins
5. In ELISE you should see a new component in Utilities tab  
![alt text](Images/Image1.png)
6. Add this component on canvas and press "Show help" in the context menu  
![alt text](Images/Image2.png)
7. In the help you can find an example `.els` file that shows how to use this component  
![alt text](Images/Image3.png)
    * This help screen is generated from `Help\Utilities\Text\HelloUniverse\HelloUniverse.md` that you can find inside the solution
8. Open the example file
9. Put a breakpoint to `SolveInstance` method of the `Hello Universe` component  
![alt text](Images/Image4.png)
10. Adjust the inputs on canvas to debug the component  
![alt text](Images/Image5.png)  
![alt text](Images/Image6.png)

## Contents of the project

* `AdvancedComponent.addin` - metadata of the add-in
* `Properties\AssemblyInfo.cs` - metadata of the project
* `Components\Utilities\Text\HelloUniverse.cs` - `Hello Universe` component
* `Controls\Utilities\Text\HelloUniverseControl.cs` - component controls of the component
* `Icons\` - icons for the add-in and components
* `Help\Utilities\Text\HelloUniverse\HelloUniverse.md` - help documentation of the component
* `Resources.resx` - resources file with user visible strings (name of the component, inputs, outputs, etc.)