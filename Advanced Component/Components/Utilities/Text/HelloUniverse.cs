﻿using Elise.Core.Graph.Data;
using Elise.Core.Graph.Enums;
using Elise.Core.Implementation.Graph;
using Elise.Core.Implementation.UI;
using Elise.DataTypes;
using Elise.Localization;
using System;
using System.Runtime.InteropServices;

namespace AdvancedComponent.Components.Utilities.Text
{
    [Guid("1DF017C6-4BED-409F-8023-FF2E964E7E6C")]
    public sealed class HelloUniverse : Component
    {
        private enum DisplayLanguage
        {
            English,
            German,
            French,
            Dutsch,
            Ukrainian,
            Alien
        }

        public HelloUniverse()
            : base(new LocalizableString(nameof(Resources.HelloUniverseName), typeof(Resources)))
        {
            Category = Categories.Utilities;
            Subcategory = Subcategories.Utilities.Text;
            Description = new LocalizableString(nameof(Resources.HelloUniverseDescription), typeof(Resources));
            GuiPriority = 1;

            InputParameterManager.AddParameter<EliseInt>(
                new LocalizableString(nameof(Resources.HelloUniverseLanguageName), typeof(Resources)),
                new LocalizableString(nameof(Resources.HelloUniverseLanguageDescription), typeof(Resources)),
                ParameterAccess.Item,
                new EliseInt(0));
            InputParameters[0].HasDynamicDefaultGraphData = true;

            InputParameterManager.AddParameter<EliseBool>(
                new LocalizableString(nameof(Resources.HelloUniverseNewLineName), typeof(Resources)),
                new LocalizableString(nameof(Resources.HelloUniverseNewLineDescription), typeof(Resources)),
                ParameterAccess.Item,
                defaultValue: new EliseBool(true));
            InputParameters[1].HasDynamicDefaultGraphData = true;

            InputParameterManager.AddParameter<EliseInt>(
                new LocalizableString(nameof(Resources.HelloUniverseSpacesName), typeof(Resources)),
                new LocalizableString(nameof(Resources.HelloUniverseSpacesDescription), typeof(Resources)),
                ParameterAccess.Item,
                defaultValue: new EliseInt(0));
            InputParameters[2].HasDynamicDefaultGraphData = true;

            OutputParameterManager.AddParameter<EliseString>(
                new LocalizableString(nameof(Resources.HelloUniverseTextName), typeof(Resources)),
                new LocalizableString(nameof(Resources.HelloUniverseTextDescription), typeof(Resources)));
        }

        protected override void SolveInstance(IDataAccess dataAccess)
        {
            bool isDataSuccess = dataAccess.GetData(0, out EliseInt language);
            isDataSuccess &= dataAccess.GetData(1, out EliseBool newline);
            isDataSuccess &= dataAccess.GetData(2, out EliseInt spacing);

            if (!isDataSuccess)
                return;

            var message = (DisplayLanguage)language.Value switch
            {
                DisplayLanguage.English => new LocalizableString(nameof(Resources.HelloUniverseEnglishMessage), typeof(Resources)),
                DisplayLanguage.German => new LocalizableString(nameof(Resources.HelloUniverseGermanMessage), typeof(Resources)),
                DisplayLanguage.French => new LocalizableString(nameof(Resources.HelloUniverseFrenchMessage), typeof(Resources)),
                DisplayLanguage.Dutsch => new LocalizableString(nameof(Resources.HelloUniverseDutchMessage), typeof(Resources)),
                DisplayLanguage.Ukrainian => new LocalizableString(nameof(Resources.HelloUniverseUkrainianMessage), typeof(Resources)),
                DisplayLanguage.Alien => new LocalizableString(nameof(Resources.HelloUniverseAlienMessage), typeof(Resources)),
                _ => new LocalizableString(nameof(Resources.HelloUniverseDefaultMessage), typeof(Resources)),
            };

            var separator = new string(' ', spacing);
            var newLineFormat = string.Format(message, newline ? Environment.NewLine : " ");
            var newStr = string.Join(separator, newLineFormat.ToCharArray());
            dataAccess.SetData(0, newStr);
        }
    }
}