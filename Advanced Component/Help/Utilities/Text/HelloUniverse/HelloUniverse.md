## Usage

This component shows developers how to create new components with components controls in ELISE. 

![alt text](Images/Image.png "Title")

### Example File(s)
Example to demonstrate this component: ([open in a new tab](HelloUniverse.els))