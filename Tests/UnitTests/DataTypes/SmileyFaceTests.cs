﻿using CustomDataType.DataTypes;
using Elise.Geometry.DataTypes;
using Elise.UnitTesting;
using System;
using Xunit;

namespace UnitTests.DataTypes
{
    public class SmileyFaceTests : EliseTest
    {
        [Fact]
        public void AreaTest()
        {
            SmileyFace smileyFace = new SmileyFace(Plane.WorldXY, 1);
            double area = smileyFace.BaseCircle.GetArea();
            Assert.Equal(Math.PI, area, 6);
        }

        [Fact]
        public void NonPositiveRadiusTest()
        {
            SmileyFace smileyFace = new SmileyFace(Plane.WorldXY, 1);
            Assert.Throws<ArgumentException>(() => smileyFace.BaseRadius = 0);
            Assert.Throws<ArgumentException>(() => smileyFace.BaseRadius = -42);
        }

        [Fact]
        public void CircleCoincidenceTest()
        {
            Plane plane = new Plane(new Point3D(1, 2, 3), new Vector3D(1, 2, 3));
            double radius = 42;
            SmileyFace smileyFace = new SmileyFace(plane, radius);

            // By inheriting the test class from EliseTest, the GeometryKernel is easily accessible.
            var circle = GeometryKernel.CreateCircle(plane, radius).ToBody();
            Assert.Equal(0, circle.DistanceTo(smileyFace.BaseCircle), 6);
        }
    }
}
