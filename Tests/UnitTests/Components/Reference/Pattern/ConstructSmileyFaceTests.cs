﻿using CustomDataType.Components.Reference.Pattern;
using CustomDataType.DataTypes;
using Elise.DataTypes;
using Elise.Geometry.DataTypes;
using Elise.UnitTesting;
using System;
using System.Drawing;
using System.Linq;
using Xunit;

namespace UnitTests.Components.Reference.Pattern
{
    public class ConstructSmileyFaceTests : ComponentTest<ConstructSmileyFace>, IDisposable
    {
        public ConstructSmileyFaceTests()
        {
            InitializeTest();
        }

        public void Dispose()
        {
            CleanUpTest();
        }

        [Fact]
        public void NonPositiveSizeErrorTest()
        {
            // Assert that the component generates outputs with its default inputs.
            Component.Assert.Outputs[0].IsNotEmpty();
            Component.Assert.HasNoError();
            Component.Assert.HasNoWarning();

            // SetDefault simulates the user manipulating the number slider control.
            Component.SetDefault(1, new EliseDouble(0));
            AssertErrorAndNull();

            Component.SetInput(1, new EliseDouble(-42));
            AssertErrorAndNull();

            void AssertErrorAndNull()
            {
                Component.Assert.HasError("Size should be a positive number.");
                Assert.Null(Component.Output[0].GetAllData().Single());
            }
        }

        [Fact]
        public void OutputTest()
        {
            Plane plane = new Plane(new Point3D(1, 2, 3), new Vector3D(1, 2, 3));
            double radius = 42;
            Color color = Color.FromArgb(100, 120, 125);
            Component.SetInputs(plane, new EliseDouble(radius), new EliseColor(color.R, color.G, color.B));

            // IsEqualTo performs a deep comparison using ValueEquals.
            Component.Assert.Outputs[0].IsEqualTo(new SmileyFace(plane, radius, color));
        }
    }
}
