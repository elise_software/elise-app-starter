﻿using AdvancedComponent.Components.Utilities.Text;
using Elise.DataTypes;
using Elise.UnitTesting;
using System;
using System.Linq;
using Xunit;

namespace UnitTests
{
    public class HelloUniverseTests : ComponentTest<HelloUniverse>, IDisposable
    {
        public HelloUniverseTests()
        {
            // This is called before each test.
            InitializeTest();
        }

        public void Dispose()
        {
            // This is called after each test.
            CleanUpTest();
        }

        [Fact]
        public void GermanMessageTest()
        {
            // Set single inputs to the component by their index.
            Component.SetInput(0, new EliseInt(1));
            Component.SetInput(1, new EliseBool(false));
            Component.SetInput(2, new EliseInt(0));

            // Make sure the component has no warnings and errors.
            Component.Assert.HasNoError();
            Component.Assert.HasNoWarning();

            // Check if the output is as expected.
            var outputString = Component.Output[0].GetAllData().OfType<EliseString>().Single().Value;
            Assert.Equal("Hallo, Universum! Willkommen bei ELISE.", outputString);
        }

        [Fact]
        public void DutchMessageTest()
        {
            // Set all inputs in one go.
            Component.SetInputs(new EliseInt(3), new EliseBool(true), new EliseInt(0));

            // Make sure the component has no warnings and errors.
            Component.Assert.HasNoError();
            Component.Assert.HasNoWarning();

            // Check if the output is as expected.
            var outputString = Component.Output[0].GetAllData().OfType<EliseString>().Single().Value;
            Assert.Equal("Hallo, Universum!\r\nWelkom bij ELISE.", outputString);
        }
    }
}
