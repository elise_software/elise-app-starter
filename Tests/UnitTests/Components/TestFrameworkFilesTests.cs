﻿using Elise.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace UnitTests.Components
{
    public class TestFrameworkFilesTests
    {
        public static readonly IEnumerable<object[]> TestFrameworkFiles = Directory
            .EnumerateFiles(@"..\..\..\TestFrameworkFiles", "*.els", SearchOption.TopDirectoryOnly)
            .Select(f => new object[] { f });

        /// <summary>
        /// This theory creates one unit test for each test file contained
        /// in the TestFrameworkFiles folder. It runs the solution of the document
        /// and a test fails if at least one Assert component fails.
        /// </summary>
        /// <param name="file">The file to test.</param>
        [Theory]
        [MemberData(nameof(TestFrameworkFiles))]
        public void FileTest(string file)
        {
            Assert.True(EliseInstance.ExecuteTestFrameworkFile(file));
        }
    }
}