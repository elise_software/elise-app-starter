# Welcome to ELISE unit testing

## Developer portal

The full guide explaining how to automate your tests is available 
[on the developer portal](https://portal.elise.de/#/dev-docs/latest/2029813761).

## Quick start

This guide will allow you to add automated tests to your Add-in solution.

It shows you how to

* create [unit tests](https://portal.elise.de/#/dev-docs/latest/2030043137) for components and data types
* integrate [test framework files](https://portal.elise.de/#/dev-docs/latest/1764786211) into a test project

1. Make sure that ELISE is installed to `C:\Program Files\ELISE`
    * If it's installed to a different location, you need to update `LocalEliseLocation` variable the `.csproj` file and `executablePath` in `Properties/launchSettings.json`
2. Open `EliseSamples.sln`
3. Build the `UnitTests` project.
4. Open the Test Explorer via the main menu Test → Test Explorer. It should look similar to this.  
![alt text](Images/Image1.PNG)
5. Run all tests and verify that they all get a green check mark.  

## Contents of the project

* `Components` - several unit tests for the `Hello Universe` and `Construct Smiley Face` components
* `Components\TestFrameworkFilesTests.cs` - unit test for test framework files
* `DataTypes\SmileyFaceTests.cs` - unit tests for the `SmileyFace` data type