﻿using Elise.Core.Implementation.ApplicationService;
using System.Reflection;

[assembly: AssemblyEliseAddin("06E48D14-6A5A-4952-8913-1C26F9385034")] // make sure that this GUID matches the one in CustomDataType.addin file

[assembly: AssemblyTitle("Custom Data Type")]
[assembly: AssemblyVersion("1.0.0.0")]

[assembly: System.Runtime.Versioning.SupportedOSPlatform("windows10.0.15063")]