## Description

A smiley face that can be rendered in the viewer. This type shows ELISE Developers how to create custom Data Types.

## Hints

In this file you can specify important information about this data type.

## Example File(s)

Example to demonstrate this type: ([open in a new tab](SmileyFace.els))