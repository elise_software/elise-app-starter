﻿using CustomDataType.Components.Reference.Pattern;
using Elise.Wpf.DirectX.Attributes;
using Elise.Wpf.DirectX.Canvas.Controls.Component;
using Elise.Wpf.DirectX.Canvas.Controls.ComponentControls.Builders;
using Elise.Wpf.DirectX.Controls;
using Elise.Wpf.DirectX.Extensions;

namespace CustomDataType.Controls.Pattern.Reference
{
    [ViewFor(typeof(ConstructSmileyFace))]
    public sealed class ConstructSmileyFaceControl : ComponentWithComponentControls
    {
        public ConstructSmileyFaceControl(ConstructSmileyFace component)
            : base(component)
        {
        }

        protected override void ConfigureControls(StackPanel container, Builder builder)
        {
            var slider = builder
                .CreateNumberSlider()
                .AllowChangeDecimalPlaces()
                .AllowChangeMaximum()
                .KeepMinimumGreaterThan(0)
                .SetMaximum(10)
                .SetDecimalPlaces(0)
                .BuildForEliseDoubleParameter(1);

            container.AddChild(slider);
        }
    }
}
