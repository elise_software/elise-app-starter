﻿using CustomDataType.DataTypes;
using Elise.Core.Graph.Data;
using Elise.Core.Graph.Enums;
using Elise.Core.Implementation.Graph;
using Elise.Core.Implementation.UI;
using Elise.DataTypes;
using Elise.Geometry.DataTypes;
using Elise.Localization;
using System.Runtime.InteropServices;

namespace CustomDataType.Components.Reference.Pattern
{
    [Guid("53BBDE53-9647-41E5-9891-B17792593123")]
    public sealed class ConstructSmileyFace : Component
    {
        public ConstructSmileyFace()
            : base(new LocalizableString(nameof(Resources.ConstructSmileyFaceName), typeof(Resources)))
        {
            Category = Categories.Reference;
            Subcategory = Subcategories.Reference.Pattern;
            Description = new LocalizableString(nameof(Resources.ConstructSmileyFaceDescription), typeof(Resources));
            GuiPriority = 4;

            InputParameterManager.AddParameter<Plane>(
                new LocalizableString(nameof(Resources.ConstructSmileyFacePlaneName), typeof(Resources)),
                new LocalizableString(nameof(Resources.ConstructSmileyFacePlaneDescription), typeof(Resources)),
                ParameterAccess.Item,
                Plane.WorldXY);

            InputParameterManager.AddParameter<EliseDouble>(
                new LocalizableString(nameof(Resources.ConstructSmileyFaceSizeName), typeof(Resources)),
                new LocalizableString(nameof(Resources.ConstructSmileyFaceSizeDescription), typeof(Resources)),
                ParameterAccess.Item,
                defaultValue: new EliseDouble(1));
            InputParameters[1].HasDynamicDefaultGraphData = true;

            InputParameterManager.AddParameter<EliseColor>(
                new LocalizableString(nameof(Resources.ConstructSmileyFaceColorName), typeof(Resources)),
                new LocalizableString(nameof(Resources.ConstructSmileyFaceColorDescription), typeof(Resources)),
                ParameterAccess.Item,
                defaultValue: new EliseColor(246, 209, 48));

            OutputParameterManager.AddParameter<SmileyFace>(
                new LocalizableString(nameof(Resources.ConstructSmileyFaceFaceName), typeof(Resources)),
                new LocalizableString(nameof(Resources.ConstructSmileyFaceFaceDescription), typeof(Resources)));
        }

        protected override void SolveInstance(IDataAccess dataAccess)
        {
            bool isDataSuccess = dataAccess.GetData(0, out Plane plane);
            isDataSuccess &= dataAccess.GetData(1, out double size);
            isDataSuccess &= dataAccess.GetData(2, out EliseColor color);
            if (!isDataSuccess)
                return;

            if (size <= 0)
            {
                AddError(new LocalizableString(nameof(Resources.ConstructSmileyFaceWrongSizeError), typeof(Resources)));
                return;
            }

            dataAccess.SetData(0, new SmileyFace(plane, size, color));
        }
    }
}