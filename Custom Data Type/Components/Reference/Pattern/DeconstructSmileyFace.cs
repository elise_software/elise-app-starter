﻿using CustomDataType.DataTypes;
using Elise.Core.Graph.Data;
using Elise.Core.Implementation.Graph;
using Elise.Core.Implementation.UI;
using Elise.DataTypes;
using Elise.Geometry.DataTypes;
using Elise.Geometry.Geometry;
using Elise.Localization;
using System.Runtime.InteropServices;

namespace CustomDataType.Components.Reference.Pattern
{
    [Guid("BD434984-6CC0-4D9C-878E-71A7B4E4FAF2")]
    public sealed class DeconstructSmileyFace : Component
    {
        public DeconstructSmileyFace()
            : base(new LocalizableString(nameof(Resources.DeconstructSmileyFaceName), typeof(Resources)))
        {
            Category = Categories.Reference;
            Subcategory = Subcategories.Reference.Pattern;
            Description = new LocalizableString(nameof(Resources.DeconstructSmileyFaceDescription), typeof(Resources));
            GuiPriority = 4;
            IsReadonly = true;

            InputParameterManager.AddParameter<SmileyFace>(
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceFaceName), typeof(Resources)),
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceFaceName), typeof(Resources)));

            OutputParameterManager.AddParameter<IBody>(
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceHeadName), typeof(Resources)),
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceHeadDescription), typeof(Resources)));
            OutputParameterManager.AddParameter<ICircle>(
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceLeftEyeName), typeof(Resources)),
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceLeftEyeDescription), typeof(Resources)));
            OutputParameterManager.AddParameter<ICircle>(
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceRightEyeName), typeof(Resources)),
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceRightEyeDescription), typeof(Resources)));
            OutputParameterManager.AddParameter<IArc>(
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceSmileName), typeof(Resources)),
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceSmileDescription), typeof(Resources)));
            OutputParameterManager.AddParameter<EliseDouble>(
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceRadiusName), typeof(Resources)),
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceRadiusDescription), typeof(Resources)));
            OutputParameterManager.AddParameter<Plane>(
                new LocalizableString(nameof(Resources.DeconstructSmileyFacePlaneName), typeof(Resources)),
                new LocalizableString(nameof(Resources.DeconstructSmileyFacePlaneDescription), typeof(Resources)));
            OutputParameterManager.AddParameter<EliseColor>(
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceColorName), typeof(Resources)),
                new LocalizableString(nameof(Resources.DeconstructSmileyFaceColorDescription), typeof(Resources)));
        }

        protected override void SolveInstance(IDataAccess dataAccess)
        {
            bool isDataSuccess = dataAccess.GetData(0, out SmileyFace smile);
            if (!isDataSuccess)
                return;

            dataAccess.SetData(0, smile.BaseCircle);
            dataAccess.SetData(1, smile.LeftEye);
            dataAccess.SetData(2, smile.RightEye);
            dataAccess.SetData(3, smile.Smile);
            dataAccess.SetData(4, smile.BaseRadius);
            dataAccess.SetData(5, smile.Plane);
            dataAccess.SetData(6, smile.Color);
        }
    }
}