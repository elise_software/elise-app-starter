﻿using CustomDataType.DataTypes;
using Elise.Core.Implementation.Graph;
using Elise.Localization;
using System.Runtime.InteropServices;

namespace CustomDataType.Components.Data.GeometryContainer
{
    [Guid("A7C73B37-0199-4BDA-BBA8-0A214CCA1BA5")]
    public sealed class SmileyFaceContainer : Container<SmileyFace>
    {
        public SmileyFaceContainer()
            : base(new LocalizableString(nameof(Resources.SmileyFaceContainerName), typeof(Resources)))
        {
            Subcategory = Elise.Core.Implementation.UI.Subcategories.Data.GeometryContainer;
            GuiPriority = 6;
        }
    }
}
