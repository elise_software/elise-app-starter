# Welcome to ELISE component development

## Developer portal

Full guide explaining canvas data types is available
[on the developer portal](https://portal.elise.de/#/dev-docs/latest/1465516074).

## Quick start

This guide will allow you to add a new `DataType` to ELISE.

It shows you how to

* create [canvas data types](https://portal.elise.de/#/dev-docs/latest/1465516074)
* create a [container](https://portal.elise.de/#/docs/latest/2127822865) for this data type
* add [icons](https://portal.elise.de/#/dev-docs/latest/1614905369) for data types
* create [help files](https://portal.elise.de/#/dev-docs/latest/1607401502) for data types

1. Make sure that ELISE is installed to `C:\Program Files\ELISE`
    * If it's installed to a different location, you need to update `LocalEliseLocation` variable the `.csproj` file and `executablePath` in `Properties/launchSettings.json`
2. Open `EliseSamples.sln`
3. Set `CustomDataType` as the startup project
4. Click on Run button to build the project and start debug session
    * ELISE starts automatically whenever you start debug session
    * In DEBUG mode the project is built to `C:\ProgramData\ELISE\Addins` - this is the default folder for ELISE addins
5. In ELISE you should see several new components that use `SmileyFace` data type   
![alt text](Images/Image1.png)
6. Add the container on canvas and press "Show help" in the context menu  
![alt text](Images/Image2.png)
7. In the help you can find an example `.els` file that shows how to use `SmileyFace`  
![alt text](Images/Image3.png)
    * This help screen is generated from `Help\DataTypes\SmileyFace.md` that you can find inside the solution
8. Open the example file
9. Put breakpoints to methods of `SmileyFace` and play with the canvas  
![alt text](Images/Image4.png)  

## Contents of the project

* `CustomDataType.addin` - metadata of the add-in
* `Properties\AssemblyInfo.cs` - metadata of the project
* `DataTypes\SmileyFace.cs` - `Smiley Face` `DataType`
* `Components` - several components that allow to create and inspect the aforementioned `DataType` 
* `Controls` - component controls of the components
* `Icons\` - icons for the add-in, `DataType` and components
* `Help\DataTypes\SmileyFace.md` - help documentation of the `DataType`
* `Resources.resx` - resources file with user visible strings (name of the component, inputs, outputs, etc.)