﻿using Elise.Core.Graph;
using Elise.Core.Graph.Data;
using Elise.Core.Implementation.ApplicationService;
using Elise.Core.UI;
using Elise.Core.Viewer;
using Elise.DataTypes;
using Elise.Geometry;
using Elise.Geometry.DataTypes;
using Elise.Geometry.Geometry;
using Elise.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.Serialization;
using Point3D = Elise.Geometry.DataTypes.Point3D;

namespace CustomDataType.DataTypes
{
    [DataContract]
    [Localization(ResourceKey = nameof(Resources.SmileyFaceTypeName), ResourseDictionary = typeof(Resources))]
    public class SmileyFace : IGraphDataType, IViewerObject, IFreezable
    {
        private double _baseRadius = 1;
        private Plane _plane = Plane.WorldXY;
        private Color _color = Color.FromArgb(124, 205, 16);

        [DataMember]
        public double BaseRadius
        {
            get => _baseRadius;
            set
            {
                ThrowIfFrozen();
                if (value <= 0)
                    throw new ArgumentException("The radius must be a positive number");

                _baseRadius = value;
                ComputeProperties();
            }
        }

        [DataMember]
        public Plane Plane
        {
            get => _plane;
            set
            {
                ThrowIfFrozen();
                if (value == null)
                    throw new ArgumentNullException(nameof(value));

                _plane = value;
                ComputeProperties();
            }
        }

        [DataMember]
        public Color Color
        {
            get => _color;
            set
            {
                ThrowIfFrozen();
                _color = value;
            }
        }

        public IBody BaseCircle { get; private set; }

        public ICircle LeftEye { get; private set; }

        public ICircle RightEye { get; private set; }

        public IArc Smile { get; private set; }

        public bool IsValid => BaseRadius > 0 && Plane?.IsValid == true && BaseCircle?.IsValid == true && LeftEye?.IsValid == true && RightEye?.IsValid == true && Smile?.IsValid == true;

        public bool IsFrozen { get; private set; }

        public SmileyFace()
        {
        }

        public SmileyFace(Plane plane, double radius)
        {
            _baseRadius = radius;
            _plane = plane;
            ComputeProperties();
        }

        public SmileyFace(Plane plane, double radius, Color color)
            : this(plane, radius)
        {
            Color = color;
        }

        public bool CanConvertFrom(object source)
        {
            if (source is Plane)
                return true;
            if (source is Point3D)
                return true;

            return false;
        }

        public bool TryConvertFrom(object source)
        {
            if (source is Plane plane)
            {
                Plane = plane;
                return true;
            }
            if (source is Point3D point)
            {
                Plane = new Plane(point, Vector3D.ZAxis);
                return true;
            }
            return false;
        }

        public bool CanConvertTo(Type targetType)
        {
            if (targetType.IsAssignableFrom(typeof(IBody)))
            {
                return true;
            }
            if (targetType.IsAssignableFrom(typeof(Point3D)))
            {
                return true;
            }
            return false;
        }

        public bool TryConvertTo(Type targetType, out object result)
        {
            if (targetType.IsAssignableFrom(typeof(IBody)))
            {
                result = BaseCircle.Clone();
                return true;
            }
            if (targetType.IsAssignableFrom(typeof(Point3D)))
            {
                result = BaseCircle.GetAreaCentroid();
                return true;
            }
            result = null;
            return false;
        }

        public override string ToString()
        {
            return string.Format(Resources.FormatSmileyFace, BaseRadius.ToStringFormat());
        }

        public string GetIcon()
        {
            return "SmileyFace.svg";
        }

        public bool ValueEquals(IValueEquatable other)
        {
            if (!(other is SmileyFace otherSmileyFace))
                return false;

            return BaseRadius == otherSmileyFace.BaseRadius
                && Plane.ValueEquals(otherSmileyFace.Plane)
                && Color == otherSmileyFace.Color;
        }

        public IEnumerable<int> GetDeepHashCodes()
        {
            yield return BaseRadius.GetHashCode();
            yield return Color.GetHashCode();
            yield return HashUtilities.HashEnumerable(Plane.GetDeepHashCodes());
        }

        public object Clone()
        {
            return new SmileyFace((Plane)Plane.Clone(), BaseRadius, Color);
        }

        public ViewerData GetViewerData(ViewerDataOptions options)
        {
            ViewerData data = new ViewerData();
            data.Merge(ViewerData.ReferenceGeometry.CreatePoint(LeftEye.GetCenter().X, LeftEye.GetCenter().Y, LeftEye.GetCenter().Z));
            data.Merge(ViewerData.ReferenceGeometry.CreatePoint(RightEye.GetCenter().X, RightEye.GetCenter().Y, RightEye.GetCenter().Z));
            data.Merge(ViewerData.ReferenceGeometry.CreatePoint(BaseCircle.GetAreaCentroid().X, BaseCircle.GetAreaCentroid().Y, BaseCircle.GetAreaCentroid().Z));
            data.Merge(LeftEye.GetViewerData(options));
            data.Merge(RightEye.GetViewerData(options));
            data.Merge(Smile.GetViewerData(options));

            var baseCircle = BaseCircle.GetViewerData(options);
            foreach (var face in baseCircle.Faces)
            {
                var vertexColors = new List<float>();
                for (int i = 0; i < face.Vertices.Count / 3; i++)
                {
                    vertexColors.Add(Color.R / 255f);
                    vertexColors.Add(Color.G / 255f);
                    vertexColors.Add(Color.B / 255f);
                    vertexColors.Add(Color.A / 255f);
                }
                face.VertexColors = vertexColors;
            }
            data.Merge(baseCircle);
            return data;
        }

        public void Freeze()
        {
            if (IsFrozen)
                return;

            // freeze any freezable members
            IsFrozen = true;
        }

        private void ComputeProperties()
        {
            var gKernel = Application.Current.KernelManager.Get<IGeometryKernel>();
            var eyeRadius = BaseRadius / 4;
            var lEyeOrigin = Plane.PointAt(BaseRadius * 0.3, BaseRadius * 0.3);
            var rEyeOrigin = Plane.PointAt(BaseRadius * -0.3, BaseRadius * 0.3);
            BaseCircle = gKernel.CreateTrimmedPlanarSheets(new ICurve[] { gKernel.CreateCircle(Plane, BaseRadius) })[0];
            LeftEye = gKernel.CreateCircle(new Plane(lEyeOrigin, Plane.Normal), eyeRadius);
            RightEye = gKernel.CreateCircle(new Plane(rEyeOrigin, Plane.Normal), eyeRadius);
            Smile = gKernel.CreateArc(Plane, BaseRadius / 2d, new Interval1D(3.5, 6d));
        }

        private void ThrowIfFrozen()
        {
            if (IsFrozen)
            {
                throw new ObjectFrozenException();
            }
        }
    }
}
